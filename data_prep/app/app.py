"""
This is the implementation of data preparation for sklearn
"""

import os
import sys
import numpy as np
import pandas as pd
from sklearn import linear_model
from sklearn.model_selection import train_test_split

from xpresso.ai.core.data.pipeline.abstract_pipeline_component import \
    AbstractPipelineComponent
from xpresso.ai.core.logging.xpr_log import XprLogger
from sklearn.externals import joblib

__author__ = "Reyaz"

logging = XprLogger("data_prep")

INR = 2.75
UR = 5.3

class DataPrep(AbstractPipelineComponent):

    """ Main class for any pipeline job. It is extended from AbstractPipelineComponent
    which allows xpresso platform to track and manage the pipeline.
    User will need to implement following method:
       -start: This is where the main functionality of the component is initiated.
          This method has a single parameter - the experiment run ID. This is automatically
          passed by xpresso.ai as the first argument when the component is run
        -completed: this is called when the main functionality of the component
          is complete, and results are to be stored if required.

    """
    def __init__(self, file_path):
        super().__init__(name="DataPrep")
        """ Initialize all the required constansts and data here"""
        self.dataset = pd.read_excel(file_path)

        self.validation_data = pd.read_excel(file_path, usecols=["Interest_Rate","Unemployment_Rate","Stock_Index_Price"])

        self.x = self.dataset[['Interest_Rate',
                               'Unemployment_Rate']]  # here we have 2 variables for multiple regression. If you just want to use one variable for simple linear regression, then use X = df['Interest_Rate'] for example.Alternatively, you may add additional variables within the brackets

        self.y = self.dataset['Stock_Index_Price']

        self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.x, self.y, test_size=0.2)

        self.predictions = None

        self.model = linear_model.LinearRegression()


    def start(self, run_name, new_interest_rate=INR, new_unemployment_rate=UR):
        super().start(xpresso_run_name=run_name)

        self.model.fit(self.X_train, self.y_train)

        print('Intercept: \n', self.model.intercept_, flush=True)

        print('Coefficients: \n', self.model.coef_, flush=True)

        self.new_interest_rate = new_interest_rate

        self.new_unemployment_rate = new_unemployment_rate

        self.predictions = self.model.predict([[self.new_interest_rate, self.new_unemployment_rate]])

        self.prediction_store = self.predictions[:, np.newaxis]

        print('Predicted Stock Index Price: \n', self.predictions, flush=True)

        self.completed()

    def send_metrics(self, status, predicted_values_y):
        report_status = {"status": {"status": status},
                         "metric": {
                             "rows": predicted_values_y.shape[0],
                             "columns": predicted_values_y.shape[1]}
                         }
        self.report_status(status=report_status)

    def completed(self, push_exp=True):

        """ Data processing is completed. Saving the output in a file and
                      persisting the state """

        # self.send_metrics("Predicted Stock Index price:", self.prediction_store)
        # output_dir = "/data/sample_data/"

        # if not os.path.exists(output_dir):
        #     os.makedirs(output_dir)
        # self.prediction_store.to_csv(os.path.join(output_dir, "y_mlr.csv"), index=False)
        # print("Output saved", flush=True)
        #
        # if not os.path.exists(output_dir):
        #     os.makedirs(output_dir)
        # self.validation_data.to_csv(os.path.join(output_dir, "test_stock.csv"), index=False)

        logging.info("Data saved")

        logging.info("Saving model")


        if not os.path.exists(self.OUTPUT_DIR):
            os.makedirs(self.OUTPUT_DIR)
        # Save the model as a pickle in a file
        joblib.dump(self.model, os.path.join(self.OUTPUT_DIR, 'save_model_mlr.pkl'))
        logging.info("Saved model... load model")

        super().completed(push_exp=push_exp)

    def terminate(self):
        """
        This is used to shutdown the current pipeline execution. All the
        component in the pipeline will be terminated. Once terminated, the
        current pipeline execution cannot be restarted later.

        """
        # === Your start code base goes here ===
        super().terminate()

    def pause(self, push_exp=True):
        """
        Pause method is used to pause the execution of the job so that it
        can be restarted at some later point. User should implement this function
        to save the state of the current execution. This state will be used
        on restart.
        Args:
            push_exp: Whether to push the data present in the output folder
               to the versioning system. This is required once training is
               completed and model needs to be versioned
        """
        # === Your start code base goes here ===
        super().pause()

    def restart(self):
        """
        Restart method is used to start any previously paused experiment. It
        starts the experiment from the same state which was stored when pause
        experiment was called. This should implement the logic to
        reload the state of the previous run.
        """
        # === Your start code base goes here ===
        super().restart()


if __name__ == "__main__":
    # To run locally. Use following command:
    # XPRESSO_PACKAGE_PATH=$PWD/../xpresso_ai enable_local_execution=true python app/app.py

    data_prep = DataPrep(file_path="/data/sample_data/mlr_data.xlsx")
    if len(sys.argv) >= 2:
        data_prep.start(run_name=sys.argv[1],new_interest_rate=int(sys.argv[2]), new_unemployment_rate=int(sys.argv[3]))
    else:
        data_prep.start(run_name="")



